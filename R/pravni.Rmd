---
title: "Firmy, které ministerstvu financí fakturovaly právní služby"
author: "petr.koci@samizdat.cz"
date: "30. ledna 2015"
output:
  html_document:
    fig_width: 9
    theme: united
    toc: yes
---


```{r echo=FALSE, message=FALSE}
library(dplyr)
library(xtable)
library(knitr)
library(DT)
library(tidyr)
library(lubridate)
library(ggplot2)
```
## Celková útrata za právní služby

V letech 2010 - 2015 utratilo ministerstvo financí za externí právní služby celkem 672 milionů korun. V ministerstvem [zveřejněné databázi](http://data.mfcr.cz/dataset/prehled-faktur-ministerstva-financi-cr) jsme našli 1891 faktur na právní služby od 117 dodavatelů. Jak se celková útrata měnila za působení jednotlivých ministrů?

Nejvíce ministerstvo za externí právní služby utrácelo za ministra Kalouska, v průměru 14,2 milionu korun měsíčně. Kalousek byl zároveň ze všech sledovaných ministrů v úřadu nejdéle, celková útrata za externí právní služby přesáhla během jeho působení půl miliardy korun. Nejméně, průměrně 4,2 milionu měsíčně, platil úřad externím právníkům za působení Jana Fischera. Všechna čísla přehledně v tabulce:

```{r echo = FALSE, results = "asis"}
vysledek1 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T))

triditko <- function(datum) {
        if (datum<as.Date("2010-07-13")) {return("Janota")}
        else if (datum<as.Date("2013-07-10")&datum>as.Date("2010-07-12")) {return("Kalousek")}
        else if (datum<as.Date("2014-01-29")&datum>as.Date("2013-07-09")) {return("Fischer")}
        else if (datum>as.Date("2014-01-28")) {return("Babiš")}
}

ministr <- character()

for (i in vysledek1$datum_uhrady) {
        ministr <- append(ministr, triditko(as.Date(i)))
}

vysledek1 <- cbind(vysledek1, ministr)

vysledek1 <- vysledek1 %>%
        group_by(ministr) %>%
        summarise(celkem = sum(castka))

vysledek1 <- cbind(vysledek1, mesice=c(11.17043, 6.63655, 6.308008, 35.8768))

vysledek1 <- vysledek1 %>%
        mutate(mesicni_prumer = celkem/mesice) %>%
        arrange(desc(mesicni_prumer))
kable(vysledek1, caption="Útraty ministerstva financí za externí právní služby", col.names=c("ministr", "celkový objem proplacených faktur", "počet měsíců ve funkci", "průměrná měsíční útrata"))
```

Následující graf pak ukazuje objem proplacených faktur po měsících.

```{r echo = FALSE, results = "asis"}
mesice <- data.frame(
        datum = c(seq(as.Date("2010-01-01"), by="month", length=60), seq(as.Date("2010-02-01"), by="month", length=60) - 1)
)
mesice <- mesice %>%
        mutate(mesic=paste0(year(datum), formatC(month(datum), width=2, flag=0)))

vysledek1 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T))

vysledek <- vysledek1 %>%
        mutate(uhrada = as.Date(datum_uhrady)) %>%
        filter(uhrada<as.Date("2015-01-01")) %>%
        mutate(mesic=paste0(year(uhrada), formatC(month(uhrada), width=2, flag=0))) %>%
        group_by(mesic) %>%
        summarise(celkem=sum(castka))

vysledek <- merge(vysledek, mesice, by = "mesic", all = T)

p <- ggplot(vysledek, aes(x=datum, y=celkem/1000000)) + geom_line(size= 1, color = "red")
                        p <- p + geom_vline(xintercept=14803, size=0.7, linetype="dotted")
                        p <- p + geom_vline(xintercept=15896, size=0.7, linetype="dotted")
                        p <- p + geom_vline(xintercept=16099, size=0.7, linetype="dotted")
                        p <- p + annotate("text", x=as.Date("2010-07-13"), y=0, label="Kalousek", vjust=1.4, hjust=-0.1, size=4)
                        p <- p + annotate("text", x=as.Date("2013-07-10"), y=0, label="Fischer", vjust=1.4, hjust=-0.1, size=4)
                        p <- p + annotate("text", x=as.Date("2014-01-29"), y=0, label="Babiš", vjust=1.4, hjust=-0.1, size=4)
                        p <- p + labs(x="", y="miliony korun")
                        p <- p + ggtitle("Měsíční objemy proplacených faktur za právní služby")
                        p


```

## Všechny faktury za právní služby
Ze všech faktur vybíráme ty, které mají v předmětu fakturace slova "právní", "zastupování", "soudní", "spor", "kauza", "žaloba"... 

Upozornění: Celkové součty ukazují pouze vybrané faktury, tj. ty, kde jsou v předmětu fakturace zmíněná slova. Celkové objemy fakturace jednotlivých provozovatelů lze dohledat na https://samizdat.cz/data/faktury-mfcr/www/datatable.html či https://samizdat.shinyapps.io/faktury/  

```{r echo=FALSE, results="asis"}
vysledek1 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T)) %>% 
        select(datum_uhrady, dodavatel, ucel, castka) %>%
        arrange(desc(castka))
datatable(vysledek1)
```

## Dodavatelé právních služeb podle celkového objemu fakturace
```{r echo=FALSE, results="asis"}
vysledek2 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T)) %>% 
        group_by(dodavatel) %>%
        summarise(celkem=(sum(castka))) %>%
        arrange(desc(celkem))
kable(vysledek2)
```

## Dodavatelé právních služeb podle celkového objemu fakturace za působení jednotlivých ministrů
```{r echo=FALSE, results="asis"}
vysledek3 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T))


triditko <- function(datum) {
        if (datum<as.Date("2010-07-13")) {return("Janota")}
        else if (datum<as.Date("2013-07-10")&datum>as.Date("2010-07-12")) {return("Kalousek")}
        else if (datum<as.Date("2014-01-29")&datum>as.Date("2013-07-09")) {return("Fischer")}
        else if (datum>as.Date("2014-01-28")) {return("Babiš")}
}

ministr <- character()

for (i in vysledek3$datum_uhrady) {
        ministr <- append(ministr, triditko(as.Date(as.character(i))))
}

vysledek3 <- cbind(vysledek3, ministr)

vysledek3 <- vysledek3 %>%
        group_by(dodavatel, ministr) %>%
        summarise(celkem=sum(castka))

vysledek3 <- spread(vysledek3, ministr, celkem)

vysledek3[is.na(vysledek3)] <- 0

vysledek3 <- vysledek3 %>%
        select(dodavatel, Janota, Kalousek, Fischer, Babiš) %>%
        mutate(celkem=Janota+Kalousek+Fischer+Babiš) %>%
        arrange(desc(celkem))

kable(vysledek3)
```

## Dodavatelé právních služeb za působení jednotlivých ministrů - průměrné měsíční útraty
```{r echo=FALSE, results="asis"}
vysledek4 <- faktury %>%
        filter(!grepl("dětí", ucel, T) & !grepl("právní nástupce", ucel, T) & !grepl("delimitační", ucel, T) & !grepl("dotace", ucel, T) & !grepl("předplatné", ucel, T) & !grepl("časopis", ucel, T) & !grepl("časopis", ucel, T) & !grepl("překlad", ucel, T) & !grepl("databáze", ucel, T) & !grepl("SAGIT", dodavatel, T) & !grepl("IGNUM, s.r.o.", dodavatel, T) & !grepl("literatura", ucel, T) & !grepl("školení", ucel, T) & !grepl("e-FRACTAL, s.r.o.", dodavatel, T) & !grepl("Audit., daň. a porad. služby", ucel, T) & !grepl("Syscom software s.r.o.", dodavatel, T) & !grepl("Olomoucký kraj", dodavatel, T) & !grepl("Nakladatelství C. H. Beck, s. r. o.", dodavatel, T) & !grepl("WME Data a.s.", dodavatel, T) & !grepl("Wolters Kluwer ČR, a.s.", dodavatel, T) & !grepl("Syntaxit, s.r.o.", dodavatel, T) & !grepl("Středočeský kraj", dodavatel, T) & !grepl("CET 21 spol.s r.o.", dodavatel, T) & !grepl("InterCatering spol. s.r.o", dodavatel, T) & !grepl("OECD", dodavatel, T) & !grepl("Demontáž a transport archivních regálů", ucel, T) & !grepl("VOJANOVY SADY s.r.o.", dodavatel, T) & !grepl("Nakladatelství C.H.BECK", dodavatel, T) & !grepl("IMPRESS Art Design sro.", dodavatel, T) & !grepl("Státní tiskárna cenin", dodavatel, T) & !grepl("KONEX s.r.o.", dodavatel, T) & !grepl("AGC Automotive Replacement Glass Czech s. r. o.", dodavatel, T) & !grepl("TULI-FORM s.r.o.", dodavatel, T) & !grepl("Martin Hrubý", dodavatel, T) & !grepl("Nákup DDHM", ucel, T) & !grepl("KRISTYÁN cz s.r.o.", dodavatel, T) & !grepl("stanovení výše majetkového obohacení neoprávněným užívbáním", ucel, T) & !grepl("Novatrix", dodavatel, T) & !grepl("SEND Předplatné s.r.o.", dodavatel, T) & !grepl("Hexnerová Ivana, RNDr.", dodavatel, T) & !grepl("Neoluxor s.r.o.", dodavatel, T)) %>% 
        filter(grepl("právn", ucel, T) | grepl("zastupov", ucel, T) | grepl("žalob", ucel, T) | grepl("soudní", ucel, T) | grepl("kauza", ucel, T) | grepl("spor", ucel, T))

triditko <- function(datum) {
        if (datum<as.Date("2010-07-13")) {return("Janota")}
        else if (datum<as.Date("2013-07-10")&datum>as.Date("2010-07-12")) {return("Kalousek")}
        else if (datum<as.Date("2014-01-29")&datum>as.Date("2013-07-09")) {return("Fischer")}
        else if (datum>as.Date("2014-01-28")) {return("Babiš")}
}

ministr <- character()

for (i in vysledek4$datum_uhrady) {
        ministr <- append(ministr, triditko(as.Date(as.character(i))))
}

vysledek4 <- cbind(vysledek4, ministr)

vysledek4 <- vysledek4 %>%
        group_by(dodavatel, ministr) %>%
        summarise(celkem=sum(castka))

vysledek4 <- spread(vysledek4, ministr, celkem)

vysledek4[is.na(vysledek4)] <- 0

vysledek4 <- vysledek4 %>%
        mutate(celkem=(Janota+Kalousek+Fischer+Babiš)/59.95893)%>%
        mutate(Janota=Janota/6.308008) %>%
        mutate(Kalousek=Kalousek/35.8768) %>%
        mutate(Fischer=Fischer/6.63655) %>%
        mutate(Babiš=Babiš/11.17043) %>%
        arrange(desc(celkem)) %>%
        select(dodavatel, Janota, Kalousek, Fischer, Babiš, celkem)

kable(vysledek4)
```