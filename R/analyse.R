library(stringr)
library(dplyr)
library(ggplot2)
require(grid)
library(jsonlite)
library(lubridate)
library(shiny)

# get data

f2010 <- read.csv("../data/Uhrazene-faktury_2010.csv", header = F, sep = ";", skip = 1, dec=",")
f2011 <- read.csv("../data/Uhrazene-faktury_2011.csv", header = F, sep = ";", skip = 1, dec=",")
f2012 <- read.csv("../data/Uhrazene-faktury_2012.csv", header = F, sep = ";", skip = 1, dec=",")
f2013 <- read.csv("../data/Uhrazene-faktury_2013.csv", header = F, sep = ";", skip = 1, dec=",")
f2014 <- read.csv("../data/Uhrazene-faktury_2014.csv", header = F, sep = ";", skip = 1, dec=",")

faktury <- rbind(f2010, f2011, f2012, f2013, f2014)

names(faktury) <- c("cislo_faktury", "dodavatel", "cislo_faktury_dodavatele", "typ_dokladu", "vs", "castka", "mena", "datum_vystaveni", "datum_prijeti", "datum_splatnosti", "datum_uhrady", "ucet_dodavatele", "ucel", "uhrada_cizi_mena", "castka_cizi_mena", "cizi_mena_id", "cislo_radku", "castka_bez_dph_radek", "castka_radek")


write.csv(faktury, file="../data/faktury_komplet.csv", row.names=F)


# data vyčištěná v Open Refine
options(scipen=999)
faktury <- read.csv("../data/faktury_komplet_refine.csv")
faktury <- tbl_df(faktury)

# celkový objem a počet faktur
dodavatele <- faktury %>%
        group_by(dodavatel) %>%
        summarise(s=sum(castka), c=n()) %>%
        arrange(desc(s))

# export hlavní tabulky        
dodavatele$i <- paste0("<img src='../img/", seq(1, 3861), ".png'>")
names(dodavatele)[1] <- "d"

writeLines(toJSON(dodavatele), "../data/dodavatele.json")


# grafy průběhu fakturace po měsících
mesice <- data.frame(
        datum = c(seq(as.Date("2010-01-01"), by="month", length=61), seq(as.Date("2010-02-01"), by="month", length=61) - 1),
        castka = rep_len(0, 61)
)
mesice <- mesice %>%
        mutate(mesic=paste0(year(datum), formatC(month(datum), width=2, flag=0)))
c <- 1

for (i in dodavatele$d) {
        vysledek <- data %>%
                filter(dodavatel==i) %>%
                mutate(mesic=paste0(year(datum), formatC(month(datum), width=2, flag=0))) %>%
                group_by(mesic, dodavatel) %>%
                summarise(castka=sum(castka))
        for (j in unique(mesice$mesic)) {
                        if(nrow(vysledek[vysledek$mesic==j, ])<1) {
                                vysledek <- rbind(vysledek, data.frame(mesic=j, dodavatel=vysledek$dodavatel[1], castka=0))                        
                        }                
                }        
        vysledek <- merge(mesice, vysledek, by="mesic", all=T)        
        
        p <-  ggplot(vysledek, aes(x=datum, y=castka.y)) + geom_line(color="red", size=2)
        p <- p + geom_vline(xintercept=14803, size=1, linetype="dashed")
        p <- p + geom_vline(xintercept=15896, size=1, linetype="dashed")
        p <- p + geom_vline(xintercept=16099, size=1, linetype="dashed")
        p <- p + labs(x=NULL, y=NULL)
        p <- p + theme(axis.title=element_blank(),
                       text=element_blank(),
                       title=element_blank(),
                       axis.ticks=element_blank(),
                       rect=element_blank(),
                       line=element_blank(),
                       plot.margin=unit(c(0,0,0,0), "mm"))
        
        ggsave(p, filename=paste0("../www/img/", c, ".png"), width=7, height=2)
        
        print(i)
        print(c)
        c <- c + 1
}


# před Babišem a po Babišovi

f <- faktury %>%
        filter(dodavatel=="VZP ČR")
BabisCelkem <- f %>%
        filter(as.Date(datum_uhrady)>as.Date("2014-01-29")) %>%
        summarise(sum(castka))
NeBabisCelkem <- f %>%
        filter(as.Date(datum_uhrady)<as.Date("2014-01-29")) %>%
        summarise(sum(castka))
print(BabisCelkem)
print(BabisCelkem/11)
print(NeBabisCelkem)
print(NeBabisCelkem/49)


# export dat pro Shiny app
data <- faktury %>%
        select(dodavatel, castka, datum_uhrady, ucel)
save(data, file = "faktury/data.RData")
