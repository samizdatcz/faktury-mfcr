require! {
  fs
  async
}

toId = -> it.toLowerCase!replace /[^-a-z0-9]/g '-'

lines = fs.readFileSync "#__dirname/../data/faktury_komplet_refine.tsv" .toString!split "\n"
  ..shift!
  ..pop!
out = {}
for line in lines
  [cislo, dodavatel, _, _, _, castka, _, _, _, _, uhradil, _, ucel] = line.split "\t"
  castka = parseFloat castka
  ucel .= replace /["]{2,}/g '"'
  ucel .= replace (new RegExp '^"' 'g'), ' '
  ucel .= replace /"$/g ""
  out[dodavatel] ?= []
  out[dodavatel].push {castka, uhradil, ucel}

out_arr = for dodavatel, data of out
  id = toId dodavatel
  {id, data}

async.eachLimit out_arr, 5, ({id, data}, cb) ->
  <~ fs.writeFile "#__dirname/../data/dodavatele/#id.json", JSON.stringify {data}
  cb!

