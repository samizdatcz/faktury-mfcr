numberFormat =
  display: -> "#{ig.utils.formatNumber it}&nbsp;Kč"
dateFormat =
  display: ->
    it.split "-" .reverse!join ". "
linkFormat =
  display: ->
    '<a href="#">' + it + '</a>'
$body = $ 'body'
$ '#overview' .DataTable do
  "order": [[1, "desc"]]
  "ajax": if window.location.hash == '#pravnici' then "dodavatele_pravnici.json" else "dodavatele.json",
  "deferRender": true,
  "columns": [
    { "data" : "d", render: linkFormat},
    { "data" : "s", render: numberFormat}
    { "data" : "c" },
    { "data" : "i", bSortable: false }
  ],
  "language" :
    "url" : "https://cdn.datatables.net/plug-ins/a5734b29083/i18n/Czech.json"

$detailContainer = $ '#detail-container'
$overviewContainer = $ '#overview-container'
originalDetailTable = $detailContainer.html!
lastScrollTop = null
displayDetail = (id) ->
  $body.addClass "detail"
  lastScrollTop := $body.scrollTop!
  $body.scrollTop 0
  $detailContainer.html originalDetailTable
  $ '#detail' .DataTable do
    "ajax": "../data/dodavatele/#id.json",
    "deferRender": true,
    "columns": [
      { "data" : "ucel"},
      { "data" : "castka", render: numberFormat},
      { "data" : "uhradil", render: dateFormat}
    ],
    "language" :
      "url" : "https://cdn.datatables.net/plug-ins/a5734b29083/i18n/Czech.json"

detailDisplayed = no
$document = $ document
$document .on \click '#overview tr td:first-child a' ->
  it.preventDefault!
  id = @innerText
    .toLowerCase!
    .replace /[^-a-z0-9]/g '-'
  displayDetail id
  detailDisplayed := yes
  window.location.hash = id
  <~ setTimeout _, 800
  $overviewContainer.addClass "hidden"
  notifyHeight!


$document .on \click \.backbutton (evt) ->
  evt.preventDefault!
  window.location.hash = ''
  detailDisplayed := no
  notifyHeight!
  $body.removeClass 'detail'
  $overviewContainer.removeClass "hidden"
  $body.scrollTop lastScrollTop

notifyHeight = ->
  h = getHeight! + 50
  if window.top.postMessage
    window.top.postMessage h, '*'
  else
    window.location.hash = 'h'+h

$document .on \change \select notifyHeight

if window.location.hash and window.location.hash != '#pravnici'
  displayDetail that.slice 1


getHeight = ->
  $parent = if detailDisplayed
    $detailContainer
  else
    $overviewContainer
  $parent.height!
